# Calculating pdistance between sequences with degenerated bases

[base degeneracy codes](https://eu.idtdna.com/pages/products/custom-dna-rna/mixed-bases):

## 2-base:
|code|bases|
|-|-|
R | A,G 
Y | C,T 
M | A,C 
K | G,T 
S | C,G
W | A,T

## pdist coefficients:
 #| A   | C   | G   | T   | R   | Y   | M   | K   | S   | W   |
|-| -   | -   | -   | -   | -   | -   | -   | -   | -   | -   |
A | 0   | 1   | 1   | 1   | 0.5 | 1   | 0.5 | 1   | 1   | 0.5
C | 1   | 0   | 1   | 1   | 1   | 0.5 | 0.5 | 1   | 0.5 | 1  
G | 1   | 1   | 0   | 1   | 0.5 | 1   | 1   | 0.5 | 0.5 | 1  
T | 1   | 1   | 1   | 0   | 1   | 0.5 | 1   | 0.5 | 1   | 0.5
R | 0.5 | 1   | 0.5 | 1   | 0   | 1   | 0.5 | 0.5 | 0.5 | 0.5
Y | 1   | 0.5 | 1   | 0.5 | 1   | 0   | 0.5 | 0.5 | 0.5 | 0.5
M | 0.5 | 0.5 | 1   | 1   | 0.5 | 0.5 | 0   | 1   | 0.5 | 0.5
K | 1   | 1   | 0.5 | 0.5 | 0.5 | 0.5 | 1   | 0   | 0.5 | 0.5
S | 1   | 0.5 | 0.5 | 1   | 0.5 | 0.5 | 0.5 | 0.5 | 0   | 1  
W | 0.5 | 1   | 1   | 0.5 | 0.5 | 0.5 | 0.5 | 0.5 | 1   | 0  

## \>2-base:
|code|bases|
|-|-|
H | A,C,T 
B | C,G,T 
V | A,C,G 
D | A,G,T 
N | A,C,G,T

