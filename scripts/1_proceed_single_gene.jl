#!/usr/bin/env julia
using DrWatson
@quickactivate "pdist_degen_bases"

using XLSX, DataFrames, FastaIO

include(srcdir("common_funcs.jl"))
using .common_funcs

# From Excel table
function extract_alleles(df::DataFrame, gene=GENE)
    sample_ids = parse.(Int, df.Samples)
    alleles = map(
        x -> parse.(Int, split(replace(String(x), "ref"=>"0"), '/')), # where 0 means reference gene
        df[!, gene]
    )
    return (sample_ids, alleles)
end

function alleles_of_1_gene_to_sepfile(filename::String, gene=GENE)
    out_path = datadir("ONLY:"*gene * "_" * basename(filename))

    FastaReader(filename) do f_in
        FastaWriter(out_path) do f_out

            for (desc, seq) in f_in
                if !occursin(gene, desc)
                    continue
                else
                    id = parse(Int, replace(split(desc, '_')[end], "ref"=>'0'))
                    writeentry(f_out, string(id), seq)
                end
            end

        end
    end
    return out_path
end

function Mafft_align(file_in::String)
    file_out = datadir("ALGN:" * basename(file_in))
    
    # Accuracy-oriented method (https://mafft.cbrc.jp/alignment/software/manual/manual.html)
    # 'physbio' is the name of conda env, where mafft is installed
    run(pipeline(`conda run -n physbio mafft --globalpair --maxiterate 1000 $file_in`, stdout = file_out))
    return file_out
end

function FastaToDict(filename::String)
    res = Dict{Int, String}()

    FastaReader(filename) do f
        for (name, seq) in f
            res[parse(Int, name)] = seq
        end
    end

    return res
end

const FILE_XLSX = projectdir("_research", "Genotypes_ori.xlsx")
const FILE_FASTA = projectdir("_research", "Galega_ori_all_GTs.fasta")
const GENE = "ori_NORK_exon3"

function main()
    ori_df = DataFrame(XLSX.readtable(FILE_XLSX, 1))
    samples, alleles = extract_alleles(ori_df, GENE)

    aligned_fasta = FILE_FASTA |> alleles_of_1_gene_to_sepfile |> Mafft_align
    aligned_dict = aligned_fasta |> FastaToDict

    consensus_seqs = [(aligned_dict[a], aligned_dict[b]) for (a,b) in alleles] .|> consensus
    write_new_dset(consensus_seqs, datadir("CONS:" * basename(aligned_fasta)), string.(samples))
end

if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end


