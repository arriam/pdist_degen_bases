#!/bin/bash

# Specify input and output directories
input_dir="_research/real"
output_dir="_research/real/algn"

# Loop through all .fasta files in the input directory
for file in ${input_dir}/*.fasta; do
    # Extract the base file name without the extension
    base_name=$(basename $file .fasta)

    # Run mafft on each file and output to the corresponding file in the output directory
    conda run -n physbio mafft --globalpair --maxiterate 1000 $file > ${output_dir}/${base_name}_algn.fasta
done