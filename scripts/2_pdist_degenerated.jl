#!/usr/bin/env julia
using DrWatson
@quickactivate "pdist_degen_bases"

using FastaIO
using DelimitedFiles


const PD_SCORE = [
    # R: A,G
    # Y: C,T
    # M: A,C
    # K: G,T
    # S: C,G
    # W: A,T

    #A    C    G    T    R    Y    M    K    S    W
    0    1    1    1    0.5  1    0.5  1    1    0.5 # A
    1    0    1    1    1    0.5  0.5  1    0.5  1   # C
    1    1    0    1    0.5  1    1    0.5  0.5  1   # G
    1    1    1    0    1    0.5  1    0.5  1    0.5 # T
    0.5  1    0.5  1    0    1    0.5  0.5  0.5  0.5 # R
    1    0.5  1    0.5  1    0    0.5  0.5  0.5  0.5 # Y
    0.5  0.5  1    1    0.5  0.5  0    1    0.5  0.5 # M
    1    1    0.5  0.5  0.5  0.5  1    0    0.5  0.5 # K
    1    0.5  0.5  1    0.5  0.5  0.5  0.5  0    1   # S
    0.5  1    1    0.5  0.5  0.5  0.5  0.5  1    0   # W
]
const ALPHABET= Dict(
    '-'=>0x0, 'N'=>0x0, 'A'=>0x1, 'C'=>0x2,
    'G'=>0x3, 'T'=>0x4, 'R'=>0x5, 'Y'=>0x6,
    'M'=>0x7, 'K'=>0x8, 'S'=>0x9, 'W'=>0xa,
)


function FastaToRam(filename::String, n_max::Int)
    sequences = Vector{Vector{UInt8}}(undef, n_max)
    names = Matrix{String}(undef, 1, n_max)

    FastaReader(filename) do f
        for ((name, seq), i) in zip(f, 1:n_max)
            sequences[i] = toint(seq, ALPHABET)
            names[1, i] = split(name, ' ')[1]
        end
    end

    return (sequences, names)
end

function toint(seq::String, alphabet::Dict)

    res = Vector{UInt8}(undef, length(seq))
    @inbounds for i in eachindex(seq)
        res[i] = alphabet[seq[i]]
    end
    return res
end


function pdist_degen(seq1, seq2, scoreMatrix)
    diff = 0  
    total = 0
    @inbounds @simd for i in eachindex(seq1, seq2) # no conditional branches
        x = seq1[i] * seq2[i] != 0                 # true if both nucleotides are present (no `-` or `N`)
        diff += x && scoreMatrix[seq1[i], seq2[i]] # if so find score in matrix
        total += x                                 # and add 1 to total
    end
    total > 0 ? diff/total : 0  
end


function p_matrix(sequences, n_max)
    matrix = zeros(Float32, n_max, n_max)
    seq1 = sequences[1]
    seq2 = sequences[1]
    for i = 1:n_max
        seq1 = sequences[i]
        for j = i+1:n_max
            seq2 = sequences[j]
            dist = pdist_degen(seq1, seq2, PD_SCORE)
            matrix[i, j]  = matrix[j, i] = dist
        end
    end
    return matrix
end


function main()
    @assert length(ARGS)==1 "Need 1 cl arg: fasta name"
    fasta_name = ARGS[1]
    
    if endswith(fasta_name, ".fasta")
        n_max = parse(Int, readchomp(`grep -c '>' $(ARGS[1])`))
        inRAM, names = FastaToRam(ARGS[1], n_max)
        pdMatrix = p_matrix(inRAM, n_max)

        matrix_name = datadir("pMTR:" * split(basename(fasta_name), '.')[1] * ".csv")
        writedlm(matrix_name, [names; pdMatrix], ',')

        println("New file $matrix_name has been written!")
    else
        println("Enter valid fasta file name!")
    end
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end