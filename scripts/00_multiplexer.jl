#!/usr/bin/env julia
# CLARGS:
#   filename::String
#   n_max::Int

# TAKES FIRST SEQUENCE FROM GIVEN FASTA FILE AND MULTIPLEXES IT FOR DIPLOID SET OF Genotypes_ori

using DrWatson
@quickactivate "pdist_degen_bases"

using FastaIO

include(srcdir("common_funcs.jl"))
using .common_funcs



function read_single_seq(fname)
    FastaReader(fname) do fr
        return first(fr)[2]
    end
end

function flip_nucl(seq, flip_prob=0.01)
    letters = setdiff(Set(seq), 'N')
    gapped = Char[]
    for s in seq
        if rand() < flip_prob
            push!(gapped, rand(setdiff(letters, s)))
        else
            push!(gapped, s)
        end
    end
    return join(gapped)
end


function generate_pairs(reference, n_sampl, pFlip_grand=5e-2, pFlip_smol=5e-3)
    # Ploidy == 2
    pairs=Vector{Tuple{String, String}}(undef, n_sampl)
    for i in 1:n_sampl
        sample = flip_nucl(reference, pFlip_grand)
        pairs[i] = (sample, flip_nucl(sample, pFlip_smol))
    end
    return pairs
end


function main()
    @assert length(ARGS)==2 "Need 2 cl args: fasta name and n_max"
    fasta_name = ARGS[1]
    n_max = parse(Int, ARGS[2])
    
    if endswith(fasta_name, ".fasta")
        mult_file = datadir("mult_2x$n_max:" * basename(fasta_name))
        cons_file = datadir("cons_2x$n_max:" * basename(fasta_name))
        
        rRNA = read_single_seq(fasta_name)
        pairs = generate_pairs(rRNA, n_max)

        mult_names = string.([1:2*n_max;])
        cons_names = [mult_names[i]*"_"*mult_names[i+1] for i in 1:2:2*n_max]

        mult_dset = reduce(vcat, collect.(pairs))
        cons_dset = consensus.(pairs)

        write_new_dset(mult_dset, mult_file, mult_names)
        println("New file $mult_file has been written!")
        
        write_new_dset(cons_dset, cons_file, cons_names)
        println("New file $cons_file has been written!")

    else
        println("Enter valid fasta file name and n_max!")
    end
    
end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch err
        @error err
    end
end