#!/usr/bin/env julia
using DrWatson
@quickactivate "pdist_degen_bases"

include(srcdir("common_funcs.jl"))
using .common_funcs

using XLSX, DataFrames, FastaIO


# allfasta = read_all_fasta("_research/real")
# names_ml = filter(startswith("Ml"), collect(keys(allfasta)))

excel_name = "_research/real/Medicago_geneotypes.xlsx"
df = DataFrame(XLSX.readtable(excel_name, 1))
gene_names = names(df)[2:end] .|> x->x[1:end-4]

gene_consensuses = Dict{String, Vector{String}}()

for gene in gene_names
    gene_consensuses[gene] = String[]

    # Seqs have to be aligned first! Else crash at consensus() call
    alleles = FastaToDict("_research/real/algn/" * gene * "_algn.fasta")
    col = df[!, gene*"_ref"]
    pairs = collect(split.(col, '/'))
    for (i, pair) in enumerate(pairs)
        p = replace.(pair, "0"=>"ref")
        n1, n2 = gene*"_ref_gt_" .* p
        println(length.([alleles[n1], alleles[n2]]))
        c = consensus((alleles[n1], alleles[n2]))
        push!(gene_consensuses[gene], c)
    end
    # Now gene_consensuses is filled, keys are gene names and vals contain all the consesuses across all pairs (in order from xlsx table)
    # All vals are of the same length
end



# To be implemented for any XLSX file
# names_tp = filter(startswith("Tp"), collect(keys(allfasta)))
