module common_funcs

using DrWatson
@quickactivate "pdist_degen_bases"

export consensus, write_new_dset, read_all_fasta, FastaToDict

using FastaIO

const enum_char = Dict(
    'A' => 1,
    'C' => 2,
    'G' => 3,
    'T' => 4,
    'N' => 5,
    '-' => 6,
)
const TRANSLATOR = [
#    A   C   G   T   N   -
    'A' 'M' 'R' 'W' 'A' '-'# A
    'M' 'C' 'S' 'Y' 'C' '-'# C
    'R' 'S' 'G' 'K' 'G' '-'# G
    'W' 'Y' 'K' 'T' 'T' '-'# T
    'A' 'C' 'G' 'T' 'N' '-'# N
    '-' '-' '-' '-' '-' '-'# -
]

function consensus(pair, translator=TRANSLATOR)
    l1, l2 = length.(pair)
    @assert l1 == l2 "Sequence lengths have to match for calculating consensus. You have $l1 and $l2"

    seq1, seq2 = uppercase.(pair)
    cons = Char[]

    for i in eachindex(seq1)
        i, j = enum_char[seq1[i]], enum_char[seq2[i]]
        push!(cons, translator[i, j])
    end
    return join(cons)
end


function write_new_dset(dset, fname, names)
    @assert length(dset) == length(names)
    FastaWriter(fname) do fw
        for (id, seq) in enumerate(dset)
            writeentry(fw, names[id], seq)
        end
    end
end

function FastaToDict(filename::String)
    res = Dict{String, String}()
    FastaReader(filename) do f
        for (name, seq) in f
            res[name] = seq
        end
    end
    return res
end

function read_all_fasta(dirname::String)
    @assert isdir(dirname) "dir name invalid"
    res = Dict{String, String}()
    for fasta_i in filter(endswith(".fasta"), readdir(dirname, join=true))
        merge!(res, FastaToDict(fasta_i))
    end
    res
end

end # module